package com.example.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.activities.AttendanceActivity;
import com.example.app.adapters.ClassListAdapter;
import com.example.app.adapters.SchoolDataPagerAdapter;
import com.example.app.asynctasks.HttpGetRequests;
import com.example.app.interfaces.CallbackListener;
import com.example.app.interfaces.ClickListener;
import com.example.app.models.Class;
import com.example.app.models.School;
import com.example.app.models.Student;
import com.example.app.util.ChildViewClickListener;
import com.example.app.models.Calendar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//import org.parceler.Parcels;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.TimeZone;

import static com.example.app.util.Constants.GET_DAILY_ATTENDANCE;
import static com.example.app.util.Constants.GET_MONTHLY_ATTENDANCE;
import static com.example.app.util.Constants.REQUEST_ATTENDANCE_INFO;
import static com.example.app.util.DateUtils.setDate;
import static com.example.app.util.DateUtils.setOnlyDate;


public class AttendanceFragment extends Fragment implements ClickListener, CallbackListener {

    private ArrayList<Class> mClassList;
    private ClassListAdapter mAdapter;
    private Class mSelectedclass;
    private CallbackListener mListener;
    private int mRequestcode;

    //add parameter for appropriate request code
    public static AttendanceFragment newInstance (ArrayList<Class> classes, int code){
        AttendanceFragment fragment = new AttendanceFragment();
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Class>>() {}.getType();
        String classlistString = gson.toJson(classes, type);
        bundle.putString("ClassList", classlistString);
        //bundle.putParcelable("ClassList", Parcels.wrap(classes));
        bundle.putInt("RequestCode",code);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        mListener = this;
        if (getArguments() != null) {
            Gson gson = new Gson();
            String listString = getArguments().getString("ClassList");
            if (listString != null) {
                Type type = new TypeToken<ArrayList<Class>>() {}.getType();
                mClassList = gson.fromJson(listString, type);
            }
            //mClassList = Parcels.unwrap(getArguments().getParcelable("ClassList"));
            mRequestcode = getArguments().getInt("RequestCode");
        }
    }

    @Override
    public View onCreateView(@NonNull  LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_daily_info, container, false);
        RecyclerView mClasslistview = view.findViewById(R.id.class_list);
        mClasslistview.setFocusable(true);
        mAdapter = new ClassListAdapter(getContext(), mClassList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mClasslistview.setLayoutManager(mLayoutManager);
        mClasslistview.setAdapter(mAdapter);
        mClasslistview.addOnItemTouchListener(new ChildViewClickListener( getContext(), mClasslistview, this));
        Log.v("test1", "testing this fragment");

        return view;
    }


    @Override
    public void onClick(View v, final int position) {
        final TextView classname = v.findViewById(R.id.item_name);
        classname.setFocusable(true);
        classname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.requestFocus();
                mSelectedclass = mAdapter.getSelectedClass(position);
                Log.v("selectedClass", mSelectedclass.getName());
                HttpGetRequests task = new HttpGetRequests(mRequestcode, mListener, getContext());
                Log.v("reqcode", String.valueOf(mRequestcode));
                task.execute(REQUEST_ATTENDANCE_INFO + "/" + mSelectedclass.getId() + "/" + "attendance");
            }
        });
    }

    @Override
    public void onCompletionHandler(boolean success, int requestcode, Object obj){
        if (success) {
            switch(requestcode) {
                case GET_DAILY_ATTENDANCE:
                    ArrayList<Calendar> dailycalendars = (ArrayList<Calendar>) obj;
                    mSelectedclass.getCalendars().clear();
                    for (Calendar calendar : dailycalendars) {
                        Log.v("cal",calendar.toString());
                        mSelectedclass.addCalendar(calendar);
                    }
                    Intent intent = new Intent(getContext(), AttendanceActivity.class);
                    Gson gson = new Gson();
                    Type type = new TypeToken<Class>() {}.getType();
                    String classString = gson.toJson(mSelectedclass, type);
                    intent.putExtra("selectedclass", classString);
                    startActivity(intent);
                    break;
                case GET_MONTHLY_ATTENDANCE:
                    ArrayList<Calendar> monthlycalendars = (ArrayList<Calendar>) obj;
                    mSelectedclass.getCalendars().clear();
                    for (Calendar calendar : monthlycalendars) {
                        mSelectedclass.addCalendar(calendar);
                    }
                    Intent monthlyintent = new Intent(getContext(), AttendanceActivity.class);
                    Gson newgson = new Gson();
                    Type newtype = new TypeToken<Class>() {}.getType();
                    String newClass = newgson.toJson(mSelectedclass, newtype);
                    monthlyintent.putExtra("selectedclass", newClass);
                    startActivity(monthlyintent);
                    break;
            }
        }
    }
}
