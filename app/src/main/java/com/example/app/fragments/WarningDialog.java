package com.example.app.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.app.R;

public class WarningDialog extends DialogFragment{

    private static final int AFFIRMATIVE = 0;
    private static final int NEGATIVE = 1;

    private String mWarningMessage;
    private TextView mMessage;

    private OnFragmentInteractionListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }

    }

    // Required empty public constructor
    public WarningDialog() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param message Message to be displayed in the warning dialog.
     * @return A new instance of fragment WarningDialog.
     */
    public static WarningDialog newInstance(String message) {
        WarningDialog fragment = new WarningDialog();
        Bundle args = new Bundle();
        args.putString("WarningMessage", message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mWarningMessage = getArguments().getString("WarningMessage");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_warning_dialog, null);
        mMessage = view.findViewById(R.id.warning_message);
        mMessage.setText(mWarningMessage);

        builder.setView(view)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onIteraction(AFFIRMATIVE);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onIteraction(NEGATIVE);
                    }
                });
        return builder.create();
    }


    public interface OnFragmentInteractionListener {

        void onIteraction(int resultcode);
    }
}
