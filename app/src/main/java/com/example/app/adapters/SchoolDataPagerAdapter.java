package com.example.app.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.example.app.fragments.AttendanceFragment;
import com.example.app.fragments.FeedingFragment;
import com.example.app.models.School;

import java.util.ArrayList;

public class SchoolDataPagerAdapter extends PagerAdapter {

    private String mTabTitles[] = {"Attending", "Feeding"};
    private Context mContext;
    private School mSchool;
    private final FragmentManager mFragmentManager;
    private Fragment[] mFragments;
    private int mRequestcode;


    //add parameter for daily or monthly tab
    public SchoolDataPagerAdapter(FragmentManager manager, Context context, School school, int code) {
        mFragmentManager = manager;
        mContext = context;
        mSchool = school;
        mFragments = new Fragment[2];
        mRequestcode = code;
    }

    @Override
    public int getCount() {
        return mFragments.length;
    }


    private Fragment getItem(int position) { // tabs for attendance/ feeding
        Log.v("getItem", "getItemCallTest");
        if (position == 0) {
            mFragments[0] = AttendanceFragment.newInstance(mSchool.getClasses(), mRequestcode);
        } else if (position == 1){
            mFragments[1] = FeedingFragment.newInstance(mSchool.getCalendars());
        }
        return mFragments[position];
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object fragment) {
        return ((Fragment) fragment).getView() == view;
    }
    

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabTitles[position];
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Log.v("fragment", "isitgettinghere");
        Log.v("pos", String.valueOf(position));
        Fragment fragment = getItem(position);
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.add(container.getId(), fragment, "fragment:"+position).commit();
        return fragment;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, Object object) {
        FragmentTransaction trans = mFragmentManager.beginTransaction();
        trans.remove(mFragments[position]);
        trans.commit();
        mFragments[position] = null;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
