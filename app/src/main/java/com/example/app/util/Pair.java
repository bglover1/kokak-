package com.example.app.util;

import android.os.Parcelable;
import android.util.Log;
import android.os.Parcel;

//import org.parceler.ParcelConverter;
//import org.parceler.ParcelPropertyConverter;
//import org.parceler.Parcels;
//import org.parceler.Transient;

public class Pair<E, T>{

    //@ParcelPropertyConverter(PairParcelConverter.class)

    private E mFirst;
    private T mSecond;

    public Pair() {}

    public Pair(E first, T second) {
        mFirst = first;
        mSecond = second;
    }

    public E getFirst() {
        return mFirst;
    }

    public T getSecond() {
        return mSecond;
    }

    public boolean isEquals(Pair one) {
        return (mFirst.equals(one.getFirst()) && mSecond.equals(one.getSecond()));
    }

    @Override
    public String toString() {
        return "(" + mFirst.toString() + " " + mSecond.toString() + ")";
    }

    public void setFirst(E newFirst) {
        mFirst = newFirst;
    }

    public void setSecond(T newSecond) {
        mSecond = newSecond;
    }
}
