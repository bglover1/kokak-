package com.example.app.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.app.R;
import com.example.app.asynctasks.HttpGetRequests;
import com.example.app.asynctasks.HttpPostRequests;
import com.example.app.fragments.SuccessDialog;
import com.example.app.interfaces.CallbackListener;

import java.util.HashMap;

import static com.example.app.util.Constants.POST_NEW_PASSWORD;
import static com.example.app.util.Constants.RESET_PASSWORD;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener, CallbackListener{

    private Button mSubmitButton;
    private EditText mNewPassword;
    private EditText mPassword;

    private String mContactNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.hide();

        mContactNumber = getIntent().getStringExtra("number");
        mSubmitButton = (Button) findViewById(R.id.resetpassword_submit_button);
        mNewPassword = (EditText) findViewById(R.id.confirmpassword_edittext);
        mPassword = (EditText) findViewById(R.id.newPassword_edittext);
        mSubmitButton.setOnClickListener(this);

    }


    //TODO: Set url for updating password
    @Override
    public void onClick(View view) {
        if (mNewPassword.getText().toString().compareTo(mPassword.getText().toString()) == 0) {
            HashMap<String, String> postdata = new HashMap<>();
            postdata.put("password", mNewPassword.getText().toString());
            postdata.put("number", mContactNumber);
            HttpPostRequests task = new HttpPostRequests(postdata, POST_NEW_PASSWORD, this, this);
            task.execute(RESET_PASSWORD);
        } else {
            Toast.makeText(this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
            mNewPassword.setText("");
            mNewPassword.requestFocus();
        }


    }

    @Override
    public void onCompletionHandler(boolean success, int requestcode, Object object) {
        if (success) {
            Fragment successIndicator = SuccessDialog.newInstance("Password Successfully Updated");
            getSupportFragmentManager().beginTransaction()
                    .add(successIndicator, "SuccessFragment")
                    .commit();
            Intent intent = new Intent(this, LoginActivity.class);
            //intent.putExtra("number", mContactNumber);
            startActivity(intent);
        }
    }
}
