package com.example.app.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;

import com.example.app.R;
import com.example.app.adapters.FoodDataAdapter;
import com.example.app.models.Food;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//import org.parceler.Parcels;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class FeedingActivity extends AppCompatActivity {


    private ArrayList<Food> mMeals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeding);
        Toolbar toolbar = (Toolbar) findViewById(R.id.feedingactivity_toolbar);
        setSupportActionBar(toolbar);

        Gson gson = new Gson();
        String foodlistString = getIntent().getStringExtra("selectedfood");
        if (foodlistString != null) {
            Type type = new TypeToken<ArrayList<Food>>() {}.getType();
            mMeals = gson.fromJson(foodlistString, type);
        }
        //ArrayList<Food> meals = Parcels.unwrap(getIntent().getParcelableExtra("selectedMeals"));

        String date = getIntent().getStringExtra("selecteddate");
        getSupportActionBar().setTitle(date);

        for (Food cal : mMeals) {
            Log.v("food", cal.toString());
        }

        //Log.v("feedingactfoodlist", mMeals.toString());
        ListView foodlist = (ListView) findViewById(R.id.foodList);
        FoodDataAdapter adapter = new FoodDataAdapter(this, 0, mMeals);
        foodlist.setAdapter(adapter);
    }
}
