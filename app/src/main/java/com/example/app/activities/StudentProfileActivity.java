package com.example.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.asynctasks.HttpDeleteRequests;
import com.example.app.asynctasks.HttpGetRequests;
import com.example.app.fragments.SuccessDialog;
import com.example.app.fragments.WarningDialog;
import com.example.app.interfaces.CallbackListener;
import com.example.app.models.School;
import com.example.app.models.Student;
import com.example.app.models.StudentProfile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//import org.parceler.Parcels;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.example.app.util.Constants.DELETE_STUDENT_PROFILE;
import static com.example.app.util.Constants.GET_STUDENTLIST_VIEW;
import static com.example.app.util.Constants.REQUEST_DELETE_STUDENT;
import static com.example.app.util.Constants.REQUEST_STUDENT_LIST;

public class StudentProfileActivity extends AppCompatActivity implements View.OnClickListener,
        WarningDialog.OnFragmentInteractionListener, CallbackListener {

    private final static int REQUEST_EDIT_PROFILE = 101;

    private TextView mGender;
    private TextView mSchoolID;
    private TextView mDateOfBirth;
    private TextView mGuardian;
    private TextView mTelephone;
    private TextView mNationalID;
    private TextView mAverageGrade;
    private TextView mShoesize;

    private Button mDelete;
    private Toolbar mToolbar;

    private String mDatabaseId;
    private StudentProfile mProfile;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_profile);
        mToolbar = (Toolbar) findViewById(R.id.studentprofile_toolbar);
        setSupportActionBar(mToolbar);

        Gson gson = new Gson();
        String str = getIntent().getStringExtra("StudentProfile");
        if (str != null) {
            Type type = new TypeToken<StudentProfile>() {}.getType();
            mProfile = gson.fromJson(str, type);
        }
        mDatabaseId = getIntent().getStringExtra("Id");

        mGender = (TextView) findViewById(R.id.edit_student_gender);
        mSchoolID = (TextView) findViewById(R.id.edit_student_id);
        mDateOfBirth = (TextView) findViewById(R.id.edit_student_birthdate);
        mGuardian = (TextView) findViewById(R.id.edit_student_guardian);
        mTelephone = (TextView) findViewById(R.id.edit_student_phone);
        mNationalID = (TextView) findViewById(R.id.edit_student_nationalID);
        mAverageGrade = (TextView) findViewById(R.id.edit_student_grade);
        mShoesize = (TextView) findViewById(R.id.edit_student_shoeSize);

        mDelete = (Button) findViewById(R.id.delete_student);
        mDelete.setFocusable(true);
        mDelete.setOnClickListener(this);

        populateView(mProfile);
    }

    public void populateView(StudentProfile profile) {
        String fullname = profile.getFirstName() + " " + profile.getLastName();
        getSupportActionBar().setTitle(fullname);
        mSchoolID.setText(profile.getStudentId());
        mDateOfBirth.setText(profile.getDOB());
        mGuardian.setText(profile.getGuardian());
        mGender.setText(profile.getGender());
        mTelephone.setText(profile.getTelephone());
        mNationalID.setText(profile.getNationalID());
        mAverageGrade.setText(profile.getAvegrade());
        mShoesize.setText(profile.getShoesize());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id) {
            case R.id.action_edit:
                StudentProfile currentProfile = currentProfile();
                Intent intent = new Intent (this, EditStudentProfile.class);
                Gson stdgson = new Gson();
                Type stdtype = new TypeToken<StudentProfile>() {}.getType();
                String studentString = stdgson.toJson(currentProfile, stdtype);
                intent.putExtra("CurrentStudentProfile", studentString);
                intent.putExtra("Id", mDatabaseId);
                startActivityForResult(intent, REQUEST_EDIT_PROFILE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.delete_student){
            mDelete.requestFocus();
            String name = mToolbar.getTitle().toString();
            Fragment warning = WarningDialog.newInstance("Do you want to remove " + name + " from this class?");
            getSupportFragmentManager().beginTransaction()
                    .add(warning, "Warning")
                    .commit();
        }
    }

    @Override
    public void onIteraction(int resultcode) {
        switch(resultcode) {
            case 0:
                HttpDeleteRequests task = new HttpDeleteRequests(mDatabaseId, DELETE_STUDENT_PROFILE, this, this);
                task.execute(REQUEST_DELETE_STUDENT);
                break;

            case 1:
                break;
        }
    }


    @Override
    public void onActivityResult(int requestcode, int resultcode, Intent data) {
        if (requestcode == REQUEST_EDIT_PROFILE && resultcode == Activity.RESULT_OK) {
            Log.v("seeUpdate", "gettingHere");
            Gson gson = new Gson();

            String str = data.getStringExtra("UpdatedStudentProfile");
            Log.v("seeUpdate", str);
            if (str != null) {
                Type type = new TypeToken<StudentProfile>() {}.getType();
                mProfile = gson.fromJson(str, type);
            }
            //StudentProfile result = Parcels.unwrap(data.getParcelableExtra("UpdatedStudentProfile"));
            String profilename = mProfile.getFirstName() + " " + mProfile.getLastName();
            mToolbar.setTitle(profilename);
            mGender.setText(mProfile.getGender());
            mSchoolID.setText(mProfile.getStudentId());
            mDateOfBirth.setText(mProfile.getDOB());
            mGuardian.setText(mProfile.getGuardian());
            mTelephone.setText(mProfile.getTelephone());
            mNationalID.setText(mProfile.getNationalID());
            mAverageGrade.setText(mProfile.getAvegrade());
            mShoesize.setText(mProfile.getShoesize());
        }
    }


    public StudentProfile currentProfile() {
        String[] fullname = mToolbar.getTitle().toString().split("\\s+");
        String firstname = fullname[0];
        String lastname = fullname[1];
        String gender = mGender.getText().toString();
        String id = mSchoolID.getText().toString();
        String dateOfBirth = mDateOfBirth.getText().toString();
        String guardian = mGuardian.getText().toString();
        String contact = mTelephone.getText().toString();
        String nationalid = mNationalID.getText().toString();
        String avggrade = mAverageGrade.getText().toString();
        String shoesize = mShoesize.getText().toString();
        return new StudentProfile(firstname, lastname, id, gender, dateOfBirth, guardian,
                contact, nationalid, avggrade, shoesize);
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onCompletionHandler(boolean success, int requestcode, Object obj) {
        if (success) {
            switch (requestcode) {
                case DELETE_STUDENT_PROFILE:
                    Fragment successIndicator = SuccessDialog.newInstance("Student Successfully Deleted");
                    getSupportFragmentManager().beginTransaction()
                            .add(successIndicator, "SuccessFragment")
                            .commit();
                    HttpGetRequests task = new HttpGetRequests(GET_STUDENTLIST_VIEW, this, this);
                    task.execute(REQUEST_STUDENT_LIST);

                    break;

                case GET_STUDENTLIST_VIEW:
                    ArrayList<Student> studentsList = (ArrayList<Student>) obj;
                    Intent intent = new Intent(this, ClassViewActivity.class);
                    Gson stdgson = new Gson();
                    Type stdtype = new TypeToken<ArrayList<Student>>() {}.getType();
                    String studentListString = stdgson.toJson(studentsList, stdtype);
                    intent.putExtra("Studentlist", studentListString);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        HttpGetRequests task = new HttpGetRequests(GET_STUDENTLIST_VIEW, this, this);
        task.execute(REQUEST_STUDENT_LIST);
    }

}



