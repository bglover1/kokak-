package com.example.app.activities;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.adapters.FoodRatioAdapter;
import com.example.app.asynctasks.HttpPostRequests;
import com.example.app.asynctasks.HttpPutRequests;
import com.example.app.interfaces.CallbackListener;
import com.example.app.models.Food;
import com.example.app.util.Pair;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

//import org.parceler.Parcels;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.app.util.Constants.POST_FOOD;
import static com.example.app.util.Constants.PUT_FOOD;
import static com.example.app.util.Constants.REQUEST_ADD_FOOD;
import static com.example.app.util.Constants.SHARED_PREFS_KEY;
import static com.example.app.util.DateUtils.setDate;
import static com.example.app.util.FoodRatios.populateList;

public class RiceBeansProgramActivity extends AppCompatActivity implements CallbackListener {

    private Food mRiceBeans;
    //private SharedPreferences mSharedPreferences = this.getSharedPreferences(SHARED_PREFS_KEY, MODE_PRIVATE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rice_beans_program);
        Gson gson = new Gson();
        String rbMeal = getIntent().getStringExtra("RiceBeansMeal");
        if (rbMeal != null) {
            Type type = new TypeToken<Food>() {}.getType();
            mRiceBeans = gson.fromJson(rbMeal, type);
        } else {
            Log.v("Exception", "Failed to retrieve food");
        }

        //mRiceBeans = Parcels.unwrap(getIntent().getParcelableExtra("RiceBeansMeal"));
        int attendancenum = getIntent().getIntExtra("attendancenum", 0);
        TextView mCurrentAttendance = (TextView) findViewById(R.id.ricebeans_attendance);
        mCurrentAttendance.setText(String.valueOf(attendancenum));


        TextView schoolname = (TextView) findViewById(R.id.riceactivity_schoolname);
        //schoolname.setText(mSharedPreferences.getString("school", null));

        ArrayList<Pair> mRatioPairs = new ArrayList<>();
        mRatioPairs = populateList(mRiceBeans);
        TextView mCurentDate = (TextView) findViewById(R.id. riceactivity_date);
        mCurentDate.setText(setDate());
        ListView ratioList = (ListView) findViewById(R.id.ricelist);
        FoodRatioAdapter adapter = new FoodRatioAdapter(this, mRatioPairs);
        ratioList.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        HashMap<String, String> ratios = mRiceBeans.getRatios();
        HashMap<String, Object> foodInfo = new HashMap<>();
        foodInfo.put("MealType", "RB");
        for (Map.Entry<String, String> ratio : ratios.entrySet()) {
            String val = ratio.getValue();
            foodInfo.put(ratio.getKey(), Double.valueOf(val.substring(0, 4)));
        }
        HttpPutRequests task = new HttpPutRequests(foodInfo, PUT_FOOD, this, this);
        task.execute(REQUEST_ADD_FOOD);
    }

    @Override
    public void onCompletionHandler(boolean success, int requestcode, Object object){}
}
