package com.example.app.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.app.R;
import com.example.app.adapters.SchoolDataPagerAdapter;
import com.example.app.fragments.AttendanceFragment;
import com.example.app.fragments.FeedingFragment;
import com.example.app.models.School;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class SchoolInfoActivity extends AppCompatActivity{


    private SchoolDataPagerAdapter mAdapter;
    //private School mSelectedSchool;
    private int mCurrentPageItem;
    private School mSelectedSchool;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.schoolinfo_toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState != null) {
            return;
        }
        Gson gson = new Gson();
        String schoolString = getIntent().getStringExtra("SelectedSchoolInfo");
        if (schoolString != null) {
            Type type = new TypeToken<School>() {}.getType();
            mSelectedSchool = gson.fromJson(schoolString, type);
        } else {
            Log.v("Exception", "Failed to retrieve the school's information");
        }
        //Request code; request being made
        int code = getIntent().getIntExtra("RequestCode", 0);
        String typeOfInfo = getIntent().getStringExtra("TypeOfInfo");
        TextView mSchoolName = (TextView) findViewById(R.id.type_of_info);
        mSchoolName.setText(typeOfInfo);

        getSupportActionBar().setTitle(mSelectedSchool.getSchoolName());

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mAdapter = new SchoolDataPagerAdapter(getSupportFragmentManager(), this, mSelectedSchool, code);
        mViewPager.setAdapter(mAdapter);

        TabLayout layout = (TabLayout) findViewById(R.id.sliding_tabs);
        layout.setupWithViewPager(mViewPager);

        ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                mCurrentPageItem = position;
                mViewPager.setCurrentItem(position);
            }

        };

        mViewPager.addOnPageChangeListener(pageChangeListener);
        pageChangeListener.onPageSelected(mCurrentPageItem);

        mAdapter.startUpdate(mViewPager);
        AttendanceFragment mAttendanceFragment = (AttendanceFragment) mAdapter.instantiateItem(mViewPager, 0);
        FeedingFragment mFeedingFragment = (FeedingFragment) mAdapter.instantiateItem(mViewPager, 1);
        mAdapter.finishUpdate(mViewPager);
    }
}

