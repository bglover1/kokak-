package com.example.app.models;

import java.util.ArrayList;


//@org.parceler.Parcel
public class Class {

    String mClassName;

    String mId;

    int mClassSize;

    String mHomeroomTeacher;

    ArrayList<Calendar> mClasscalendar;

    public Class(){}

    public Class(String name, String id, int size) {
        mClassName = name;
        mId = id;
        mClassSize = size;
        mHomeroomTeacher = "";
        mClasscalendar = new ArrayList<>();
    }


    public Class(String name, String id, int size, String teacher) {
        mClassName = name;
        mId = id;
        mClassSize = size;
        mHomeroomTeacher = teacher;
        mClasscalendar = new ArrayList<>();
    }

    public String getName() {
        return mClassName;
    }

    public String getId() {
        return mId;
    }


    public int getClassSize() {
        return mClassSize;
    }

    public String getTeacher() { return mHomeroomTeacher; }

    public ArrayList<Calendar> getCalendars() {
        return mClasscalendar;
    }

    public void addCalendar(Calendar calendar) {
        mClasscalendar.add(calendar);
    }
}
