require('dotenv').config();
const express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	passport = require('passport');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


//Mongoose==============================================================================================================================
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URI);

//Routes============================================================================================================================================================
require('./config/passport')(passport);
const authenticate = require('./routes/authenticate'),
	organization = require('./routes/organization'),
	teacher = require('./routes/teacher'),
	cook = require('./routes/cook');

app.use('/', authenticate);
app.use('/organization', organization(passport));
app.use('/teachers', teacher(passport));
app.use('/cook', cook(passport));



//Connect===========================================================================================================================================================
app.listen(process.env.PORT || 3000, function(req, res) {
	console.log("Listening on " + process.env.PORT || 3000);
})


//Schedule===========================================================================================================================================================
var schedule = require('node-schedule');
var School = require('./models/School');
var Class = require('./models/Class');
var Calendar = require('./models/Calendar');
var Attendance = require('./models/Attendence');


var rule = new schedule.RecurrenceRule();
rule.minute = 0;
rule.hour = 0;

var update = schedule.scheduleJob(rule, async function(){
	const currDate = new Date();
	const midNight = new Date(Math.floor(currDate.getTime() / (1000*60*60*24))*1000*60*60*24);
	var schools = await School.find();
	var classes = await Class.find();
	for (var i=0; i<schools.length; i++) {
		var newCalendar = new Calendar({
			date: midNight,
			school: schools[i]._id
		})
		var savedCalendar = await newCalendar.save();
	}
	for (var i=0; i<classes.length; i++) {
		var newAttendance = new Attendance({
			date: midNight,
			school: classes[i].school,
			class: classes[i]._id,
			studentsNotPresent: classes[i].students
		})
		var savedAttendance = await newAttendance.save();
	}
});
