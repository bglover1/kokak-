const express = require('express'),
	router = express.Router(),
	jwt = require('jsonwebtoken'),
	moment = require('moment'),
	School = require('./../models/School'),
	Class = require('./../models/Class'),
	Calendar = require('./../models/Calendar'),
	Attendence = require('./../models/Attendence');

module.exports = function(passport) {
	router.get('/schools', function(req,res) {
		let getSchools = School.find().select('_id name').exec();
		getSchools.then(function(schools) {
			let schoolsList = [];
			schools.forEach(function(school){
				schoolsList.push({
					id: school._id,
					name: school.name
				})
			})
			return res.status(200).json({schools: schoolsList});
		}).catch(function(error) {
			return res.status(400).json({message: "Error in fetching from database"});
		})
	})

	router.get('/schools/:id/food', function(req,res) {
		let getCalendars = Calendar.find({school: req.params.id}).sort({date: 1}).exec();
		getCalendars.then(function(calendars) {
			let daily = {},
				monthly = {},
				dates = [],
				months = [];
			for (let i=0; i<calendars.length; i++) {
				let date = moment(calendars[i].date).format('LL').split(",")[0]
			 	let month = moment(calendars[i].date).format('LL').split(",")[0].split(" ")[0]
			 	daily[date] = JSON.parse(JSON.stringify([calendars[i].uji, calendars[i].githeri, calendars[i].rb]));
			 	dates.push(date)
			 	if (months.indexOf(month)==-1) {
			 		months.push(month)
			 	}
			 	if (Object.keys(monthly).indexOf(month)>-1) {
			 		monthly[month][0]["Flour"] += calendars[i].uji.Flour;
			 		monthly[month][0]["Sugar"] += calendars[i].uji.Sugar;
			 		monthly[month][0]["Water"] += calendars[i].uji.Water;
			 		monthly[month][1]["Maize"] += calendars[i].githeri.Maize;
			 		monthly[month][1]["Beans"] += calendars[i].githeri.Beans;
			 		monthly[month][1]["Salt"] += calendars[i].githeri.Salt;
			 		monthly[month][1]["Oil"] += calendars[i].githeri.Oil;
			 		monthly[month][2]["Rice"] += calendars[i].rb.Rice;
			 		monthly[month][2]["Beans"] += calendars[i].rb.Beans;
			 		monthly[month][2]["Salt"] += calendars[i].rb.Salt;
			 		monthly[month][2]["Oil"] += calendars[i].rb.Oil;
			 	} else {
			 		monthly[month] = JSON.parse(JSON.stringify([calendars[i].uji, calendars[i].githeri, calendars[i].rb]));					 			 		
			 	}
			}
			return res.status(200).json({"dates": dates, "months": months, "daily": daily, "monthly": monthly});
		}).catch(function(error) {
			return res.status(400).json({message: "Error in fetching from database"});
		})
	})

	router.get('/schools/:id/classes', function(req,res) {
		let getSchool = School.findById(req.params.id).exec();
		let getClasses = Class.find({school: req.params.id}).select('_id name students').exec();
		Promise.all([getSchool,getClasses]).then(function(values) {
			let school = values[0];
			let classes = values[1].map(function(singleClass) {
				return {id: singleClass._id, name: singleClass.name, size: Math.trunc(singleClass.students.length)};
			})
			return res.status(200).json({school: school.name , classes: classes});
		}).catch(function(error) {
			return res.status(400).json({message: "Error in fetching from database"});
		})
	})

	router.get('/classes/:id/attendance', function(req,res) {
		let getAttendences = Attendence.find({class: req.params.id}).populate({path: 'class', select: 'name'}).sort({date: 1}).exec();
		getAttendences.then(function(attendences) {
			let responseBody = {"dates": [], "months": [], "daily": {}, "monthly": {}},
				monthCounter;			
			for (let i=0; i<attendences.length; i++) {
	           	let date = moment(attendences[i].date).format('LL').split(",")[0];
			 	let month = moment(attendences[i].date).format('LL').split(",")[0].split(" ")[0];
			 	responseBody["dates"].push(date)
			 	if (responseBody["months"].indexOf(month)==-1) {
			 		responseBody["months"].push(month)
			 	}			 	
			 	responseBody["daily"][date] = attendences[i].studentsPresent.length;
			 	if (Object.keys(responseBody["monthly"]).indexOf(month)>-1) {
		 			responseBody["monthly"][month] = (responseBody["monthly"][month]*monthCounter + attendences[i].studentsPresent.length)/(monthCounter+1);
		 			monthCounter += 1;
			 	} else {
			 		responseBody["monthly"][month] = attendences[i].studentsPresent.length;
			 		monthCounter = 1;
			 	}
			}
			return res.status(200).json(responseBody);
		}).catch(function(error) {
			return res.status(400).json({message: "Error in fetching from database"});
		})
	})		

	return router;

}
