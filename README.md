# Kotak

This app will help track student attendance and help formulate quantities of foods to cook based off the student attendance taken. This helps us massively track the impact of our programs in helping reduce absenteeism in schools in the slums and rural areas.

Generously created by the students of Haverford University for the Rahul Kotak Foundation.

## For Developers

### To Install

- Clone this repository.
- cd into the repository copy local.properties.example to local.properties.
- Install Android Studio.
- Import the repository as a gradle project in Android studio and allow it to set your SDK path in local.properties
- Accept any license agreements
- Create a test device Tools->AVD Manager
- Run->run 'kokak-app'
- Install the most recent version of node
- cd into the server sub-project
- run `npm --install`
- run `npm start`

You should see something like:

```
Listening on 3000
```



